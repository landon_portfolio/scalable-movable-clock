
# include <iostream>
# include "app_window.h"
using namespace std;

void AppWindow::idle()
{
	/*
	const time_t now = time(NULL);
	struct tm *tm_struct = localtime(&now);
	int seconds = tm_struct->tm_sec;
	//Sleep(1000);	
	const time_t now2 = time(NULL);
	struct tm *tm_struct2 = localtime(&now2);
	int seconds2 = tm_struct2->tm_sec;
	if (seconds != seconds2)
		*/
		redraw();
}

AppWindow::AppWindow ( const char* label, int x, int y, int w, int h )
          :GlutWindow ( label, x, y, w, h )
 {
   _markx = 0;
   _marky = 0;
   addMenuEntry ( "Option 0", evOption0 );
   addMenuEntry ( "Option 1", evOption1 );
						//x,y,rad, rgb, smh;
   int oldTimeSinceStart = 0;
   stash.add(new Clock());

   hit = false;
 }
// mouse events are in window coordinates, but your scene is in [0,1]x[0,1],
// so make here the conversion when needed
void AppWindow::windowToScene ( float& x, float &y )
 {
   x = (2.0f*(x/float(_w))) - 1.0f;
   y = 1.0f - (2.0f*(y/float(_h)));
 }

// Called every time there is a window event
void AppWindow::handle ( const Event& e )
 {
   bool rd=true;

   const time_t now = time(NULL);
   struct tm *tm_struct = localtime(&now);
   int seconds2 = tm_struct->tm_sec;
   int seconds1 = tm_struct->tm_sec;
   do
   {
	   //stash.first()->getTime();
   } while (seconds2 != seconds1);
   
   if (e.type == MouseDown || e.type == Motion)
   {
	   _markx = (float)e.mx;
	   _marky = (float)e.my;
	   windowToScene(_markx, _marky);
	   //if a rectangle is not already selected, and we click, draw
	   
	   if (!hit && e.type == MouseDown)
	   {
		   for (vector<Rect*> ::iterator i = stash.rects.begin(); i != stash.rects.end(); i++)
		   {
			   (*i)->selected = false;
		   }
		   //if we click over any part of another rectangle, redraw
		   for (vector<Rect*> ::iterator i = stash.rects.begin(); i != stash.rects.end(); i++)
		   {
			   //be able to select if mouse is down and moving;
			   if ((*i)->contains(Vec(_markx, _marky)))
			   {
				   Rect *temp = *i;
				   (*i)->selected = true;
				   stash.rects.erase(i);
				   stash.rects.insert(stash.rects.begin(), temp);	//insert (where, val)
				   hit = true;

				   //Vec vect = Vec(_markx - stash.first()->x, _marky - stash.first()->y);		//fix y coord
				   //stash.first()->handle(vect);
				   break;
			   }
		   }
	   }	//end !hit&&mousedown
	   
	   //create offset
	   if (e.type == MouseDown)
	   { 
		   offsetx = _markx - stash.first()->x; 
		   offsety = stash.first()->y - _marky; 
	   }

	   //drag function (only on center)
	   if (_marky <= stash.first()->y  + .1  &&			
		   _marky >= stash.first()->y - .1  &&
		   _markx <= stash.first()->x + .1 &&
		   _markx >= stash.first()->x-.1
		   && hit)
	   {
		   stash.first()->x = _markx - offsetx; 
		   stash.first()->y = _marky + offsety;
	   }

	   //resize
	  if (
		   _marky >= (stash.first()->y - stash.first()->l) &&			//above the bottom
		   _marky <= (stash.first()->y - stash.first()->l) + .20 &&		//but not too far above
		   _markx >= stash.first()->x + stash.first()->l - .20 &&		//a little before right edge
		   _markx <= stash.first()->x + stash.first()->l)				//before right edge
		{
			stash.first()->l = (_markx - (stash.first()->x)) + .05;
	   }//end resize
	   
   }//end mousedown&&motion
   

   //if unclick, you are not clicking a rect
	if (e.type == MouseUp)
		hit = false;

   if ( e.type==Menu )
    { std::cout<<"Menu Event: "<<e.menuev<<std::endl;
      rd=false; // no need to redraw
    }

   const float incx=0.02f;
   const float incy=0.02f;
   if ( e.type==SpecialKey )
    switch ( e.key )
    { case GLUT_KEY_LEFT:  _markx-=incx; break;
      case GLUT_KEY_RIGHT: _markx+=incx; break;
      case GLUT_KEY_UP:    _marky+=incy; break;
      case GLUT_KEY_DOWN:  _marky-=incy; break;
      default: rd=false; // no redraw
	}

   if (rd) redraw(); // ask the window to be rendered when possible
}	

void AppWindow::resize ( int w, int h )
 {
   // Define that OpenGL should use the whole window for rendering
   glViewport( 0, 0, w, h );
   _w=w; _h=h;
 }

// here we will redraw the scene according to the current state of the application.
void AppWindow::draw()
{
	// Clear the rendering window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Clear the trasnformation stack
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	
		for (vector<Rect*> ::iterator i = stash.rects.begin(); i != stash.rects.end(); i++)
	{
		//(*i)->generate(-1, 1, .1);
		(*i)->draw();

	}
   
	   // Swap buffers
   glFlush();         // flush the pipeline (usually not necessary)
   glutSwapBuffers(); // we were drawing to the back buffer, now bring it to the front
}

