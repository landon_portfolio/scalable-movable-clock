#include "Rect.h"
#include <vector>
#include "Vec.h"
#include <cmath>

using namespace std;

class QuadraticCurve :public Rect
{
public:
    float a1,b1,c1;

	QuadraticCurve(float x, float y, float l, float h, float r, float g, float b,float aa, float bb, float cc) :
        a1(aa),b1(bb),c1(cc){
		this->x = x;
		this->y = y;
		this->l = l;
		this->h = h;
		this->r = r;
		this->g = g;
		this->b = b;
		//
		selected = false;
	}

    void generate(float xini, float xend, float inc)
	{	
		//we want to generate in our box from 0, to L
		float x1 = 0;

		//cout << "y = " << y << endl;
		for (xini; xini <= xend; xini += (inc))
		{
			float y1 = a1*pow(xini, 2) + b1*xini + c1;
			float inc2 = (l * (inc))/2;
			y1 *= (h*2)*(inc);
			y1 -= h / 2;
			//cout << "y = " << y << "\t";
			//cout << "y1 = " <<y1 <<endl;
			Vec newV = Vec(x1, y1);
			
			if (y1 <= 0)
			eventVecs.push_back(newV);
			x1 += inc2;
		}
    }

	void draw(){
		//draw graph axis
		glBegin(GL_LINES);
	
		glColor3f(1, 1, 1);
		glVertex2d(x, y - h / 2);				//left vertex
		glVertex2d(x + l, y - h / 2);					//right
		glEnd();
		glBegin(GL_LINES);
		glColor3f(1, 1, 1);
		glVertex2d(x + l / 2, y-.05);				//"top" vertex
		glVertex2d(x + l / 2, y - h);					//bot
		glEnd();

		//white bar
		glBegin(GL_POLYGON);
		glColor3d(1, 1, 1);
		glVertex2d(x, y);								//top left
		glVertex2d(x + l, y);						//top right
		glVertex2d(x + l, y - .079);		//bottom right
		glVertex2d(x, y - .079);					//bottom left
		glEnd();

		//dots
		for (std::vector<Vec> ::iterator j = eventVecs.begin(); j != eventVecs.end(); j++)
		{
			glBegin(GL_POINTS);
			glColor3d(1, 1, 1);
			glVertex2d( ((*j).x+x),  ((*j).y+y) );
			glEnd();
		}

		//lines to each dot
		for (std::vector<Vec> ::iterator j = eventVecs.begin(); j != eventVecs.end()-1; j++)
		{
			Vec next = *j;
			glBegin(GL_LINES);
			glColor3d(1, 1, 1);
			glVertex2d(((*j).x + x), ((*j).y + y));
			glVertex2d(((*(j+1)).x + x), ((*(j+1)).y + y));
			glEnd();
		}


		//redraw area
		glBegin(GL_POLYGON);
		glColor3f(1, 1, 1);
		glVertex2d((x + l) - .10, y - h + .10);			//top left
		glVertex2d(x + l, y - h + .10);					//top right
		glVertex2d(x + l, y - h);		//bottom right
		glVertex2d(x + l - .10, y - h);					//bottom left
		glEnd();
		

		//fill in individual colored windows last
		glBegin(GL_POLYGON);
		glColor3f(r, g, b);
		glVertex2d(x, y);							//top left
		glVertex2d(x + l, y);					//top righ
		glVertex2d(x + l, y - h);		//bottom right
		glVertex2d(x, y - h);					//bottom left
		glEnd();

		//highlight around rects
		if (selected){
			glLineWidth(3);
			glBegin(GL_LINES);
			glColor3d(1, 1, 1);
			glVertex2d(x, y);				//vertex at top left
			glVertex2d(x + l, y);
			glVertex2d(x + l, y);
			glVertex2d(x + l, y - h);		//vertex at top right
			glVertex2d(x + l, y - h);	//vertex at bottom right
			glVertex2d(x, y - h);			//vertex at bottom left
			glVertex2d(x, y - h);
			glVertex2d(x, y);
			glEnd();
		}
	}

};
class CubicCurve :public Rect
{
public:
    float a2,b2,c2,d2;
	CubicCurve(float x, float y, float l, float h, float r, float g, float b, float aa, float bb, float cc, float dd) :
        a2(aa),b2(bb),c2(cc),d2(dd)
	{
		this->x = x;
		this->y = y;
		this->l = l;
		this->h = h;
		this->r = r;
		this->g = g;
		this->b = b;
		selected = false;
	}
	void add(Vec c){
		eventVecs.push_back(c);
	}

    void generate(float xini, float xend, float inc)
	{
		float x1 = 0;
		for (xini; xini <= xend; xini += inc)
		{
			float y1 = a2*pow(xini, 3) + b2*pow(xini, 2) + c2*xini +d2;
			//y1 *= (inc);
			//y1 -= (x + h) / 2;
			y1 *= (h * 2)*(inc);
			y1 -= h / 2;

			float inc2 = (l * (inc)) / 2;
			//x1 *= inc *((x+h)/2*10);
			//x1 += (x + l)/2 ;

			
			Vec newV = Vec(x1, y1);
			if (y1 <= 0)
			eventVecs.push_back(newV);

			x1 += inc2;
		}
    }
	
	void draw(){
	
		//draw graph axis
		glBegin(GL_LINES);
		glColor3f(1, 1, 1);
		glVertex2d(x, y - h / 2);				//left vertex
		glVertex2d(x + l, y - h / 2);					//right
		glEnd();
		glBegin(GL_LINES);
		glColor3f(1, 1, 1);
		glVertex2d(x + l / 2, y);				//"top" vertex
		glVertex2d(x + l / 2, y - h);					//bot
		glEnd();

		//white bar with text
		//glRasterPos2f(0, 0); // center of screen. (-1,0) is center left.
		glRasterPos2f(x+ l/2, y-.099); 
		glColor4f(1.0f, 0.55f, 1.0f, 1.0f);
	
		char tr[20];
		sprintf(tr, "CUBIC GRAPH");
		const char *p = tr;
		do
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *p);
		while (*(++p));


		glBegin(GL_POLYGON);
		glColor3d(1, 1, 1);
		glVertex2d(x, y);								//top left
		glVertex2d(x + l, y);						//top right
		glVertex2d(x + l, y - .099);		//bottom right
		glVertex2d(x, y - .099);					//bottom left
		glEnd();


		//dots
		for (std::vector<Vec> ::iterator j = eventVecs.begin(); j != eventVecs.end(); j++)
		{
			glBegin(GL_POINTS);
			glColor3d(1, 1, 1);
			glVertex2d((*j).x + x, y + (*j).y);
			glEnd();
		}

		//lines to each dot
		for (std::vector<Vec> ::iterator j = eventVecs.begin(); j != eventVecs.end() - 1; j++)
		{
			Vec next = *j;
			glBegin(GL_LINES);
			glColor3d(1, 1, 1);
			glVertex2d(((*j).x + x), ((*j).y + y));
			glVertex2d(((*(j + 1)).x + x), ((*(j + 1)).y + y));
			glEnd();
		}

		//redraw area
		glBegin(GL_POLYGON);
		glColor3f(1, 1, 1);
		glVertex2d((x + l) - .10, y - h + .10);			//top left
		glVertex2d(x + l, y - h + .10);					//top right
		glVertex2d(x + l, y - h);		//bottom right
		glVertex2d(x + l - .10, y - h);					//bottom left
		glEnd();

		//fill in individual colored windows last
		glBegin(GL_POLYGON);
		glColor3f(r, g, b);
		glVertex2d(x, y);							//top left
		glVertex2d(x + l, y);					//top righ
		glVertex2d(x + l, y - h);		//bottom right
		glVertex2d(x, y - h);					//bottom left
		glEnd();

		//highlight around rects
		if (selected){
			glLineWidth(3);
			glBegin(GL_LINES);
			glColor3d(1, 1, 1);
			glVertex2d(x, y);				//vertex at top left
			glVertex2d(x + l, y);
			glVertex2d(x + l, y);
			glVertex2d(x + l, y - h);		//vertex at top right
			glVertex2d(x + l, y - h);	//vertex at bottom right
			glVertex2d(x, y - h);			//vertex at bottom left
			glVertex2d(x, y - h);
			glVertex2d(x, y);
			glEnd();
		}

	}

	
};
	
