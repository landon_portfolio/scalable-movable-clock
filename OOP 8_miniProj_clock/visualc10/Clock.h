#pragma once
#define _USE_MATH_DEFINES
#include "Rect.h"
#include <math.h>
#include <cmath>
#include <time.h>
//#include <gl/glut.h>
#include <gl/gl.h>
#include <sys/timeb.h>

const float DEG2RAD = 3.14159 / 180;

const float angle1min = M_PI / 30.0f,
clockR = .9,
minStart = 0.9f,
minEnd = 1.0f,

stepStart = 0.8f,
stepEnd = 1.0f;


class Clock :
	public Rect
{
public: 
	//float x, y;
	//float r, g, b;
	//float radius;
	int s, m, h;
	//static float angleSec, angleMinute, angleHour;
	//float angleSec, angleMinute, angleHour ;	//degrees
	
	Clock(){
		x = 0;
		y = 0;

		//radius = .75;
		l = .75;
		h = l;

		r = .9;
		g = .5;
		b = .5;
		
		s = 0;
		m = 0;
		h = 0;

		//getAngles(angleHour , angleMinute, angleSec);
		
	}
	Clock(float x, float y, float l, 
			float r, float g, float b
			)
	{
		this->x = x;
		this->y = y;
		this->l = l;
		this->h = l;
		//this->radius = radius;
		this->r = r;
		this->g = g;
		this->b = b;
		selected = false;
	}
	~Clock();

	void getTime(){
		const time_t now = time(NULL);
		struct tm *tm_struct = localtime(&now);

		int hour = tm_struct->tm_hour;
		int minutes = tm_struct->tm_min;
		int seconds = tm_struct->tm_sec;

		s = seconds;
		m = minutes;
		h = hour;
	}

	int getHours()
	{
		return h;
	}
	int getMinutes()
	{
		return this->m;
	}
	int getSeconds()
	{
		return this->s;
	}
	void setHours(int h)
	{
		this->h = h;
	}
	void setMinutes(int s)
	{
		this->m = s;
	}
	void setSeconds(int s) { 
		this->s = s; 
	}
	void add(const Clock & t)
	{
		int t1 = t.s + this->s;
		std::cout << "t1" << t1 << std::endl;
		int counter = 0;
		while (t1 >= 60)
		{
			t1 -= 60;
			counter++;          //add minutes
		}

		int t2 = t.m + this->m;
		int cH = 0;
		while (t2 >= 60){
			t2 -= 60;
			cH++;
		}
		t2 += counter;

		int t3 = t.h + this->h;
		while (t3 >= 24)
		{
			t3 -= 12;
		}
		t3 += cH;
		setHours(t3);
		setMinutes(t2);
		setSeconds(t1);
	}
	void getAngles(float &a, float &b, float &c)
	{
		
		if (h <= 3)
			a = (M_PI) / 2 - (h*(M_PI / 6) + (m*(M_PI / 360)));
		else if (h >= 9)
			a = M_PI - (((h - 9) * (M_PI / 6) + m*(M_PI / 360)));
		else    a = -1 * (h - 3)*M_PI / 6 + m*(M_PI / 360);

		if (m <= 15)
			b = M_PI / 2 - m*(M_PI / 30);

		else if (m >= 45)
			b = (M_PI / 2 - m*(M_PI / 30));
		else
			b = -1 * (m - 15) * M_PI / 30;

		if (s <= 15)
			c = M_PI / 2 - s * M_PI / 30;
		else if (s > 45)
			c = M_PI - ((s - 45) * M_PI / 30);
		else
			c = -1 * (s - 15) *M_PI / 30;
	}

	void getHourCoords(float&x, float&y)
	{
		float hour = 0;
		if (h <= 3){
			hour = (M_PI / 2) - (h*(M_PI / 6)) + (m*(M_PI / 360));
			x = cos(hour) *l + this->x;
			y = sin(hour) *l + this->y;
		}
		else if (h >= 9){
			hour = M_PI - (((h - 9) * (M_PI / 6) + (m*(M_PI / 360))));
			x = cos(hour) *l + this->x;
			y = sin(hour) *l + this->y;
		}
		else{
			hour = -1 * ((h - 3)*(M_PI / 6) + (m*(M_PI / 360)));
			x = cos(hour) *l + this->x;
			y = sin(hour) *l + this->y;
		}
		
	}
	void getMinuteCoords(float&x, float&y){
		float minu = 0;
		if (m <= 15)
		{
			minu = M_PI / 2 - m*(M_PI / 30);
			x = cos(minu)*l + this->x  -.1;
			y = sin(minu) *l + this->y - .1 ;
		}
		else if (m >= 45){
			minu = (M_PI / 2 - m*(M_PI / 30));
			x = cos(minu)*l + this->x -.1;
			y = sin(minu) *l + this->y -.1;
		}
		else{
			minu = -1 * (m - 15) * M_PI / 30;
			x = cos(minu)*l + this->x ;
			y = sin(minu) *l + this->y ;
		}
	}
	void getSecondsCoords(float&x, float&y){
		float sec = 0;
		if (s <= 15){
			sec = M_PI / 2 - s * M_PI / 30;
			x = cos(sec)*l +this->x;
			y = sin(sec) *l + this->y;
			
		}
			
		else if (s > 45){
			sec = M_PI - ((s - 45) * M_PI / 30);
			x = cos(sec) *l+this->x;
			y = sin(sec) *l+this->y;
			
		}
		else{
			sec = -1 * (s - 15) *M_PI / 30;
			x = cos(sec)*l +this->x;
			y = sin(sec)*l + this->y;
			
		}

	}

	void newLine(float rStart, float rEnd, float angle){
		float c = cos(angle), s = sin(angle);
		glVertex2f(clockR*rStart*c, clockR*rStart*s);
		glVertex2f(clockR*rEnd*c, clockR*rEnd*s);
	}

	/*
	virtual bool contains(const Vec &v)
	{
		float r_top, r_left, r_bot, r_right;
		if (v.x == 0 && v.y == 0)
			return false;
		r_top = y+l;
		r_bot = y - l;
		r_left = x -l;
		r_right = x + l;
		return (r_top >= v.y && r_bot <= v.y && r_left <= v.x && r_right >= v.x);
	}
	*/


	virtual bool contains(const Vec &v)
	{
		float r_top, r_left, r_bot, r_right;
		if (v.x == 0 && v.y == 0)
			return false;
		r_top = y + l;
		r_bot = y - h;
		r_left = x -l;
		r_right = x + l;
		return (r_top >= v.y && r_bot <= v.y && r_left <= v.x && r_right >= v.x);
	}


	virtual void draw(){
		const time_t now = time(NULL);
		struct tm *tm_struct = localtime(&now);

		this->h = tm_struct->tm_hour;
		m = tm_struct->tm_min;
		s = tm_struct->tm_sec;

		//white bar
		glBegin(GL_POLYGON);
		glColor3d(1, 1, 1);
		glVertex2d(	x -.05, y+.05);								//top left
		glVertex2d(  x+.05, y+.05);						//top right
		glVertex2d( x+.05, y+-.05);		//bottom right
		glVertex2d( x+ -.05, y+-.05);					//bottom left
		glEnd();


		//modified code from online
		
		//@author bobo87
		//http://bobo.vyrobce.cz/index.php?id=34&n=opengl-hodiny
		glBegin(GL_LINES);
		for (int i = 0; i < 60; i++){
			if (i % 5){ // smaller 60 ticks
				if (i % 5 == 1)
					glColor3f(1.0f, 1.0f, 1.0f);
				float c = cos(i*angle1min), s = sin(i*angle1min);

				glVertex2f(l*minStart*c +x , l*minStart*s +y);
				glVertex2f(l*minEnd*c +x , l*minEnd*s +y);

			}
			else{	//12 lines
				glColor3f(1.0f, 0.0f, 0.0f);
				float c = cos(i*angle1min), s = sin(i*angle1min);
				glVertex2f(l*stepStart*c +x, l*stepStart*s +y);
				glVertex2f(l*stepEnd*c +x, l*stepEnd*s +y);
			}
		}
		glEnd();
		

		//hour hand
		float hx, hy;
		getHourCoords(hx, hy);

		glBegin(GL_LINES);
		glColor3f(1,1,1);
		glVertex2d(x,y);
		glVertex2d (hx  , hy  );
		glEnd();

		float mx, my;
		getMinuteCoords(mx, my);
		glBegin(GL_LINES);
		glColor3f(1, 1, 1);
		glVertex2d(x, y);
		glVertex2d(mx  , my );
		glEnd();

		float sx, sy;
		getSecondsCoords(sx, sy);
		glBegin(GL_LINES);
		glColor3f(0, 0, 1);
		glVertex2d(x, y);
		glVertex2d(sx , sy );
		glEnd();


		//circle
		glBegin(GL_LINE_LOOP);
		glColor3f(1, 1, 1);
		for (int i = 0; i < 360; i++)
		{
			float degInRad = i*DEG2RAD;
			glVertex2f(cos(degInRad)*l + x, sin(degInRad)*l +y );
		}
		glEnd();

		//highlight around rects
		if (selected){
			glLineWidth(3);
			glBegin(GL_LINES);
			glColor3d(1, 1, 1);
			glVertex2d(x - l, y+l);				
			glVertex2d(x +l, y + l);				//end top line				
			glVertex2d(x + l, y + l);				
			glVertex2d(x + l, y - l);				//end right line
			glVertex2d(x + l, y - l);				
			glVertex2d(x - l, y - l);				//end bottom line
			glVertex2d(x - l, y - l);				
			glVertex2d(x-l, y+l);								//end left line
			glEnd();
		}


		//redraw area
		glBegin(GL_POLYGON);
		glColor3f(1, 1, 1);
		glVertex2d( x+ l - .1, y -l+.1 );			//top left
		glVertex2d( x+ l, y -l +.1);					//top right
		glVertex2d( x+ l,  y- l );				//bottom right
		glVertex2d( x+ l - .1,y -l);				//bottom left
		glEnd();

	}
};
