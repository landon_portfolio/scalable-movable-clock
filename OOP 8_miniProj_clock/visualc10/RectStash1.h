#pragma once
#include <vector>
#include "Rect.h"
#include "plot.h"
#include "Clock.h"
class RectStash
{
public:
	std::vector <Rect*> rects;
	std::vector <Clock*> clocks;
	RectStash(){}
	Rect * first(){
		return rects[0];
	}
	Clock * clock(){ return clocks[0]; }

	void add(Rect *r){ rects.push_back(r); }
	void add(Clock *r){ rects.push_back(r); }
	~RectStash(){
		for (std :: vector<Rect*> ::iterator i = rects.begin(); i != rects.end(); i++)
			delete *i;
	}


};

