#pragma once
#include <vector>
#include <iostream>
#include "Vec.h"
#include "app_window.h"
class Rect
{
public:
	std::vector <Vec> eventVecs;
	float x = 0;
	float y = 0;
	float l = 0;
	float h = 0;
	float r = 0;
	float g = 0;
	float b = 0;
	bool selected;
	Rect()
	{
		x = 0;
		y = 0;
		l = 0;
		h = 0;
		r = 0;
		g = 0;
		b = 0;
		selected = false;
	}
	Rect(float x, float y, float l, float h, float r, float g, float b)
	{
		this->x = x;
		this->y = y;
		this->l = l;
		this->h = h;
		this->r = r;
		this->g = g;
		this->b = b;
		selected = false;
	}
	
	virtual bool contains(const Vec &v)
	{
		float r_top, r_left, r_bot, r_right;
		if (v.x == 0 && v.y==0)
			return false;
		r_top = y;
		r_bot = y - h;
		r_left = x;
		r_right = x + l;
		return (r_top >= v.y && r_bot <= v.y && r_left <= v.x && r_right >= v.x);
	}
	
	virtual void generate(float xini, float xend, float inc){}

	virtual void draw(){
	
		//white bar
		glBegin(GL_POLYGON);
		glColor3d(1, 1, 1);
		glVertex2d(x, y);								//top left
		glVertex2d(x + l, y);						//top right
		glVertex2d(x + l, y - .15);		//bottom right
		glVertex2d(x,  y  - .15);					//bottom left
		glEnd();

		//draw graph axis
		
		//dots
		for (std::vector<Vec> ::iterator j = eventVecs.begin(); j != eventVecs.end(); j++)
		{
			glBegin(GL_POINTS);
			glColor3d(1, 1, 1);
			glVertex2d((*j).x + x, y + (*j).y);
			glEnd();
		}

		//redraw area
		glBegin(GL_POLYGON);
		glColor3f(1, 1, 1);
		glVertex2d(   (x + l) -.10, y - h + .10);			//top left
		glVertex2d(x + l, y - h + .10);					//top right
		glVertex2d(x + l, y - h);		//bottom right
		glVertex2d(x + l - .10, y - h);					//bottom left
		glEnd();

		//fill in individual colored windows last
		glBegin(GL_POLYGON);
		glColor3f(r, g, b);
		glVertex2d(x, y);							//top left
		glVertex2d(x + l, y);					//top righ
		glVertex2d(x + l, y - h);		//bottom right
		glVertex2d(x, y - h);					//bottom left
		glEnd();

		//highlight around rects
		if (selected){
			glLineWidth(3);
			glBegin(GL_LINES);
			glColor3d(1, 1, 1);
			glVertex2d(x, y);				//vertex at top left
			glVertex2d(x + l, y);
			glVertex2d(x + l, y);
			glVertex2d(x + l, y - h);		//vertex at top right
			glVertex2d(x + l, y - h);	//vertex at bottom right
			glVertex2d(x, y - h);			//vertex at bottom left
			glVertex2d(x, y - h);
			glVertex2d(x, y);
			glEnd();
		}
	}

	virtual void handle(Vec &v){
		eventVecs.push_back(v);
	}

};

